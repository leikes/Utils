<?php 
namespace Leikes\Utils\Controller;

use App\Http\Controllers\Controller;
use Leikes\Utils\ModelUtil\ModelOperatorTrait;

abstract class BaseController extends Controller{
	
	use ModelOperatorTrait;

	abstract protected function model();
}