<?php
use Illuminate\Support\Facades\Cache

if(!function_exists('formatResponse')){

    /**
     * 格式化响应数据
     *
     * @param   string|int      $errCode
     * @param   string          $message
     * @param   mixed           $data
     * @return  array
     */
    function formatResponse($errCode = 0, $message = null, $data = null){

        $response = [
            'status' => [
                'errCode' => $errCode,
                'message' => $message
            ],
        ];
        

        if(!is_null($data)){
            Arr::set($response, 'data', $data); 
        }

        return $response;
    }
}

if(!function_exists('formatJsonResponse')){

    /**
     * 格式化响应数据
     *
     * @param   mixed           $data
     * @param   string          $message
     * @param   string|int      $errCode
	 * @return  \Illuminate\Http\JsonResponse
     */
    function formatJsonResponse($data = null, $message = '正常', $errCode = 0){

        
        
        return Response::json(formatResponse($errCode, $message, $data));
    }
}

if(!function_exists('generateOrderNo')){
    /**
     * 生成单据号
     *
     * @param   string          $prefix     单据前置字符
     * @param   integer         $lenght     后续数字长度
     * @return  string
     */
    function generateOrderNo($prefix = '',$lenght = 4){

        if(Cache::has($prefix.date('Ymd'))){
            $number = Cache::get($prefix.date('Ymd'));

            $number++;

            Cache::set($prefix.date('Ymd'),$number);
        }
        else{
            $number = 1;

            Cache::set($prefix.date('Ymd'),$number);
        }

        $formatNumber = sprintf('%0'. $lenght .'d', Cache::get($prefix.date('Ymd')));

        return $prefix.date('Ymd').$formatNumber
    }
}