<?php

namespace Leikes\Utils\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use Leikes\Utils\Socket\SocketBindConfig;
use Leikes\Utils\Exceptions\Socket\GenerateSocketException;

class SocketCommand extends GeneratorCommand
{

    protected $bindPlaceholder = '//:end-bindings:';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'leikes:socket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '新增leikes工具包socket链接服务';

    /**
     * The generate type.
     * @var string
     */
    protected $type = 'Socket';

    public function handle()
    {
       
        if (parent::handle() === false) {
            return;
        }

        if ($this->option('bind')) {
            $this->bind();
        }
        
    }

    protected function bind(){
         try {

            $bindClass = '\\'.$this->rootNamespace().'Sockets\\'.$this->getNameInput().'::class';

            $socketBindConfig = app(SocketBindConfig::class);

            $filePath = $socketBindConfig->getPath();

            $examplePath = app_path().'/Sockets/SocketBindConfig.php';

            $baseNamespace = 'Leikes\Utils\Socket';

            $replaceNamespace = 'App\Sockets';

            if(\File::exists($examplePath)){

                $this->replaceFileContent($examplePath,$this->bindPlaceholder,$bindClass.','. PHP_EOL . '        ' . $this->bindPlaceholder);
            }
            else{
                if(\File::isDirectory(app_path().'/Sockets/')){
                    \File::copy($filePath,$examplePath);

                    $this->replaceFileContent($examplePath,$this->bindPlaceholder,$bindClass.','. PHP_EOL . '        ' . $this->bindPlaceholder);

                    $this->replaceFileContent($examplePath,$baseNamespace,$replaceNamespace);

                }else{
                    \File::makeDirectory(app_path().'/Sockets/');

                    \File::copy($filePath,$examplePath);

                    $this->replaceFileContent($examplePath,$this->bindPlaceholder,$bindClass.','. PHP_EOL . '        ' . $this->bindPlaceholder);

                    $this->replaceFileContent($examplePath,$baseNamespace,$replaceNamespace);
                }
                
            }

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";

            if(\File::exists(app_path().'/Sockets/'.$this->getNameInput().'.php')){
                \File::delete(app_path().'/Sockets/'.$this->getNameInput().'.php');
            }

            echo "Socket deleted successfully\n";

        }
        
    }

    /**
     * Get the stub path. 
     * @Author   Leikes
     * @DateTime 2019-11-26
     * @return   [type]     [description]
     */
    protected function getStub(){
        return __DIR__.'/stubs/Socket.stub';
    }

    /**
     * Judge this exception already exists.
     * @Author   Leikes
     * @DateTime 2019-11-26
     * @param    [type]     $rawName [description]
     * @return   [type]              [description]
     */
    protected function alreadyExists($rawName)
    {
        return class_exists($this->rootNamespace().'Sockets\\'.$rawName);
    }

    /**
     * Get default namespace.
     * @Author   Leikes
     * @DateTime 2019-11-26
     * @param    [type]     $rootNamespace [description]
     * @return   [type]                    [description]
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Sockets';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['bind', 'b', InputOption::VALUE_NONE, 'Generate socket example and bind example.'],
        ];
    }

    protected function replaceFileContent($path,$baseContent,$replaceContent){
        $fileContent = \File::get($path);

        return \File::put($path,str_replace($baseContent,$replaceContent,$fileContent));
    }
}
