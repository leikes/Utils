<?php

namespace Leikes\Utils\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class ExceptionCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'leikes:exception';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '新增leikes工具包exception';

    /**
     * The generate type.
     * @var string
     */
    protected $type = 'Exception';

    /**
     * Get the stub path. 
     * @Author   Leikes
     * @DateTime 2019-11-26
     * @return   [type]     [description]
     */
    protected function getStub(){
        return __DIR__.'/stubs/Exception.stub';
    }

    /**
     * Judge this exception already exists.
     * @Author   Leikes
     * @DateTime 2019-11-26
     * @param    [type]     $rawName [description]
     * @return   [type]              [description]
     */
    protected function alreadyExists($rawName)
    {
        return class_exists($this->rootNamespace().'Exceptions\\'.$rawName);
    }

    /**
     * Get default namespace.
     * @Author   Leikes
     * @DateTime 2019-11-26
     * @param    [type]     $rootNamespace [description]
     * @return   [type]                    [description]
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Exceptions';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            
        ];
    }
}
