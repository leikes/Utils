<?php

namespace Leikes\Utils;

use Illuminate\Support\ServiceProvider;

class UtilProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/Exceptions/exceptions.default.php' => config_path('exceptions.php'), // 发布配置文件到 laravel 的config 下
            // __DIR__.'/Exceptions/exceptions.default.php' => config_path('exceptions.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            \Leikes\Utils\Commands\ExceptionCommand::class,
            \Leikes\Utils\Commands\ModelCommand::class,
            // \Leikes\Utils\Commands\SocketCommand::class,
            // \Leikes\Utils\Commands\WebSocketCommand::class,
            \Leikes\Utils\Commands\ControllerCommand::class,
            \Leikes\Utils\Commands\RequestCommand::class,
        ]);
    }
}
