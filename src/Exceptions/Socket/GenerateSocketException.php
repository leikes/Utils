<?php 

namespace Leikes\Utils\Exceptions\Socket;

use Leikes\Utils\Exceptions\BaseException;

class GenerateSocketException extends BaseException{
	public function getException()
	{
		return config('exceptions.model_util.delete_exception');
	}

	public function getMsg(){
		return '';
	}
}