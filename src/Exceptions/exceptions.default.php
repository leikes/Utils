<?php 
return [
	'model_util' => [
		'query_exception' => [
			'msg' => '查询数据异常',
			'errCode' => 20001
		],
		'store_exception' => [
			'msg' => '新增数据异常',
			'errCode' => 20002
		],
		'update_exception' => [
			'msg' => '修改数据异常',
			'errCode' => 20003
		],
		'delete_exception' => [
			'msg' => '删除数据异常',
			'errCode' => 20004
		]
	],
	'valiDator' => [
		'msg' => '数据校验异常',
		'errCode' => 20005
	],
	'socket' => [
		'generate_exception' => [
			'msg' => '生成socket实例异常',
			'errCode' => 30001
		]
	]
];