<?php 
namespace Leikes\Utils\Exceptions;

use Exception;

abstract class BaseException extends Exception
{
	public function __construct(){
		$exception = $this->getException();
		// dd($this->getMessage());

		$message = $this->getMsg();

		if($message != ''){
			$exception['msg'] = $message;
		}

		if(isset($exception) && count($exception) > 0 && array_key_exists('errCode', $exception) && array_key_exists('msg', $exception)){
			// dd(config('app.debug'));
			// if(config('app.debug')){

				parent::__construct($exception['msg'],$exception['errCode']);
			// }
			// else{
			// 	// dd('in function');

			// 	// dd(formatJsonResponse(null,$exception['msg'],$exception['errCode']));
			// 	return formatJsonResponse(null,$exception['msg'],$exception['errCode']);
			// }
			
		}else{
			parent::__construct("系统异常",10000);
		}
	}

	public abstract function getException();

	public abstract function getMsg();
}