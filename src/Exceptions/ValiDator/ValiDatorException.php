<?php
namespace Leikes\Utils\Exceptions\ValiDator;

use Leikes\Utils\Exceptions\BaseException;

class ValiDatorException extends BaseException{
	protected $msg = '';

	public function __construct($msg){

		$this->msg = $msg;

		parent::__construct();
	}

	public function getException()
	{
		return config('exceptions.valiDator');
	}

	public function getMsg(){
		return $this->msg;
	}
}
