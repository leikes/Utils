<?php 
namespace Leikes\Utils\Exceptions\ModelUtil;

use Leikes\Utils\Exceptions\BaseException;

class QueryException extends BaseException{
	public function getException():array
	{
		return config('exceptions.model_util.query_exception');
	}

	public function getMsg(){
		return '';
	}
}