<?php 
namespace Leikes\Utils\Exceptions\ModelUtil;

use Leikes\Utils\Exceptions\BaseException;

class DeleteException extends BaseException{
	public function getException():array
	{
		return config('exceptions.model_util.delete_exception');
	}

	public function getMsg(){
		return '';
	}
}