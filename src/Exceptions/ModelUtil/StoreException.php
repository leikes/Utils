<?php 
namespace Leikes\Utils\Exceptions\ModelUtil;

use Leikes\Utils\Exceptions\BaseException;

class StoreException extends BaseException{
	public function getException():array
	{
		return config('exceptions.model_util.store_exception');
	}

	public function getMsg(){
		return '';
	}
}