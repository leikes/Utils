<?php 
namespace Leikes\Utils\Exceptions\ModelUtil;

use Leikes\Utils\Exceptions\BaseException;

class UpdateException extends BaseException{
	public function getException():array
	{
		return config('exceptions.model_util.update_exception');
	}

	public function getMsg(){
		return '';
	}
}